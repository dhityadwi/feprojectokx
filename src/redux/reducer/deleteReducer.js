const initialState = {
  title: "",
  content: "",
  idDel: "",
  loadingPost: false,
  error: false,
  message: "",
};

const deleteReducer = (state = initialState, action) => {
  switch (action.type) {
    case "DELETE_POST":
      return {
        ...state,

        title: action.payload.title,
        content: action.payload.content,
        dataDel: action.payload.dataDel,
        loadingPost: false,
      };
    case "DELETEPOST/START":
      return {
        ...state,
        loadingPost: true,
      };

    case "DELETE_POST/ERROR":
      return {
        ...state,
        loadingPost: false,
        error: true,
        message: action.payload.message,
      };
    default:
      return state;
  }
};

export default deleteReducer;
