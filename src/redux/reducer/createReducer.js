const initialState = {
  title: "",
  content: "",
  loading: false,
  error: false,
  message: "",
};

const createReducer = (state = initialState, action) => {
  switch (action.type) {
    case "CREATE_POST":
      return {
        ...state,
        title: action.payload.title,
        content: action.payload.content,
        loading: false,
      };
    case "CREATE_POST/START":
      return {
        ...state,
        loadingPost: true,
      };

    case "CREATE_POST/ERROR":
      return {
        ...state,
        loadingPost: false,
        error: true,
        message: action.payload.message,
      };
    default:
      return state;
  }
};

export default createReducer;
