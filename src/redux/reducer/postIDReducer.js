const initialState = {
  data: [],
  loadingPost: false,
  error: false,
  message: "",
};

const postIDReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_POST_ID":
      return {
        ...state,
        data: action.payload.data,
        loadingPost: false,
      };
    case "GET_POST_ID/START":
      return {
        ...state,
        loadingPost: true,
      };

    case "GET_POST_ID/ERROR":
      return {
        ...state,
        loadingPost: false,
        error: true,
        message: action.payload.message,
      };
    default:
      return state;
  }
};

export default postIDReducer;
