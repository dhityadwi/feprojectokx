const initialState = {
  data: [],
  loadingPost: false,
  error: false,
  message: "",
};

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_POST":
      return {
        ...state,
        data: action.payload.data,
        loadingPost: false,
      };
    case "GET_POST/START":
      return {
        ...state,
        loadingPost: true,
      };

    case "GET_POST/ERROR":
      return {
        ...state,
        loadingPost: false,
        error: true,
        message: action.payload.message,
      };
    default:
      return state;
  }
};

export default postReducer;
