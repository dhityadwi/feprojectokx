const initialState = {
  title: "",
  content: "",
  id: "",
  loadingPost: false,
  error: false,
  message: "",
};

const editPostIDReducer = (state = initialState, action) => {
  switch (action.type) {
    case "EDIT_POST_ID":
      return {
        ...state,
        title: action.payload.title,
        content: action.payload.content,
        id: action.payload.id,
        loadingPost: false,
      };
    case "EDIT_POST_ID/START":
      return {
        ...state,
        loadingPost: true,
      };

    case "EDIT_POST_ID/ERROR":
      return {
        ...state,
        loadingPost: false,
        error: true,
        message: action.payload.message,
      };
    default:
      return state;
  }
};

export default editPostIDReducer;
