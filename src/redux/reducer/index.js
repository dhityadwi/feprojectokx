import { combineReducers } from "redux";
import createReducer from "./createReducer";
import deleteReducer from "./deleteReducer";
import editPostIDReducer from "./editPostReducer";
import postIDReducer from "./postIDReducer";
import postReducer from "./postReducer";

export const rootReducer = combineReducers({
  post: postReducer,
  postId: postIDReducer,
  createPost: createReducer,
  deletePost: deleteReducer,
  editPost: editPostIDReducer,
});
