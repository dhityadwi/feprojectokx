import { showPostService } from "../../service/postService";

export const isGetPostIDStart = () => {
  return {
    type: "GET_POST_ID/START",
  };
};

export const isGetPostID = (data) => {
  return {
    type: "GET_POST_ID",
    payload: {
      data: data,
    },
  };
};

export const isGetPostIDError = (message) => {
  return {
    type: "GET_POST_ID/ERROR",
    payload: {
      message,
    },
  };
};

export const postsIDAsync = (id) => {
  return (dispatch) => {
    dispatch(isGetPostIDStart());

    showPostService(id)
      .then((response) => {
        dispatch(isGetPostID(response.data));
        // console.log(id, "ini id");
        // console.log(response, "respon");
      })
      .catch((error) => {
        dispatch(isGetPostIDError(error.message));
      });
  };
};
