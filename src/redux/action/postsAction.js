import { getPostsService } from "../../service/postService";

export const isGetPostStart = () => {
  return {
    type: "GET_POST/START",
  };
};

export const isGetPost = (data) => {
  return {
    type: "GET_POST",
    payload: {
      data: data,
    },
  };
};

export const isGetPostError = (message) => {
  return {
    type: "GET_POST/ERROR",
    payload: {
      message,
    },
  };
};

export const postsAsync = () => {
  return (dispatch) => {
    dispatch(isGetPostStart());

    getPostsService()
      .then((response) => {
        dispatch(isGetPost(response));

        console.log(response, "respon");
      })
      .catch((error) => {
        dispatch(isGetPostError(error.message));
      });
  };
};
