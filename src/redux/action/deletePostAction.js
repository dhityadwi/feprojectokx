import { deletePostService } from "../../service/postService";

export const isDeletePostStart = () => {
  return {
    type: "DELETE_POST/START",
  };
};

export const isDeletePost = (title, content, id) => {
  return {
    type: "DELETE_POST_",
    payload: {
      title: title,
      content: content,
      idDel: id,
    },
  };
};

export const isDeletePostError = (message) => {
  return {
    type: "DELETE_POST_/ERROR",
    payload: {
      message,
    },
  };
};

export const deleteAsync = (title, content, id) => {
  return (dispatch) => {
    dispatch(isDeletePostStart());

    deletePostService(title, content, id)
      .then((response) => {
        dispatch(isDeletePost(response));
        console.log(id, "ini id del");
        console.log(response, "respon del");
      })
      .catch((error) => {
        dispatch(isDeletePostError(error.message));
      });
  };
};
