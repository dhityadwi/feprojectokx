import { putPostService } from "../../service/postService";

export const isEditPostIDStart = () => {
  return {
    type: "EDIT_POST_ID/START",
  };
};

export const isEditPostID = (title, content, id) => {
  return {
    type: "EDIT_POST_ID",
    payload: {
      title: title,
      content: content,
      id: id,
    },
  };
};

export const isEditPostIDError = (message) => {
  return {
    type: "EDIT_POST_ID/ERROR",
    payload: {
      message,
    },
  };
};

export const editPostsIDAsync = (title, content, id) => {
  return (dispatch) => {
    dispatch(isEditPostIDStart());

    putPostService(title, content, id)
      .then((response) => {
        dispatch(isEditPostID(title, content, id));
        console.log(response, "ini res ed");
      })
      .catch((error) => {
        dispatch(isEditPostIDError(error.message));
      });
  };
};
