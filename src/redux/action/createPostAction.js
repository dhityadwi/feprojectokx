import { createPostsService } from "../../service/postService";

export const isCreatePostStart = () => {
  return {
    type: "CREATE_POST/START",
  };
};

export const isCreatePost = (title, content) => {
  return {
    type: "CREATE_POST",
    payload: {
      title: title,
      content: content,
    },
  };
};

export const isCreatePostError = (message) => {
  return {
    type: "CREATE_POST/ERROR",
    payload: {
      message,
    },
  };
};

export const createPostAsync = (title, content) => {
  return (dispatch) => {
    dispatch(isCreatePostStart());

    createPostsService(title, content)
      .then((response) => {
        dispatch(isCreatePost(title, content));
        console.log(response, "ini res");
      })
      .catch((error) => {
        dispatch(isCreatePostError(error.message));
      });
  };
};
