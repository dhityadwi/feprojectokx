import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useLocation, useParams } from "react-router-dom";
import { editPostsIDAsync } from "../../redux/action/editPostAction";
import { postsIDAsync } from "../../redux/action/postIDAction";
import "../NewPost/NewPost.scss";

const EditPost = () => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

  const dispatch = useDispatch();
  const { id } = useParams();
  const { data } = useSelector((state) => state.postId);
  // console.log(data, "ini data");
  const location = useLocation();

  const HandleUpdate = (e) => {
    e.preventDefault();

    dispatch(editPostsIDAsync(title, content, id));
  };

  useEffect(() => {
    dispatch(postsIDAsync(id));
  }, []);

  return (
    <div className="your-post">
      <form onSubmit={HandleUpdate}>
        <div className="post-details">
          <h3>Create New Post</h3>
          <div className="form">
            <div className="left-form">
              <label htmlFor="">
                <p>Title*</p>
              </label>
              <input
                type="text"
                defaultValue={data.title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </div>
            <div className="right-form">
              <label htmlFor="">
                <p>Content*</p>
              </label>
              <textarea
                name="decription"
                defaultValue={data.content}
                onChange={(e) => setContent(e.target.value)}
                id=""
                placeholder="Content Decription "
              ></textarea>
            </div>
          </div>
        </div>
        <div className="button-footer">
          <button type="submit">Update</button>
        </div>
      </form>
    </div>
  );
};

export default EditPost;
