import React from "react";
import { Link } from "react-router-dom";
import "./header.scss";

const Header = () => {
  return (
    <nav className="nav">
      <label className="icon">MY APPS</label>

      <ul className="link">
        <Link to="/">
          <li>Home</li>
        </Link>
        <Link to="/new-post">
          <button className="btn">+ New Post</button>
        </Link>
      </ul>
    </nav>
  );
};

export default Header;
