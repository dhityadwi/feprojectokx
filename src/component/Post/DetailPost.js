import React, { useEffect, useState } from "react";
import { Card, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { postsIDAsync } from "../../redux/action/postIDAction";
import Loader from "./Loader";
import "./Detail.scss";
import { deleteAsync } from "../../redux/action/deletePostAction";
import { Link } from "react-router-dom";

const DetailPost = () => {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);

  const { data } = useSelector((state) => state.postId);

  console.log(data, "ini daata detail");

  const { id } = useParams();

  useEffect(() => {
    dispatch(postsIDAsync(id));
    setLoading(false);
  }, []);

  const deleteHandle = () => {
    const tittle = data.title;
    const contents = data.content;
    dispatch(deleteAsync(tittle, contents, id));
    console.log(tittle, "deltil");
  };

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <Row className="mt-5">
          <Card
            className="my-4 p-4 rounded text-center mb-3 pt-5"
            style={{ border: "none" }}
          >
            <div className="post">
              <div className="post-title">
                <h3>{data.title}</h3>
              </div>
              <Card.Body className="rounded text-white">
                <div className="post-title">
                  <p>{data.content}</p>
                </div>
              </Card.Body>
            </div>
          </Card>
        </Row>
      )}
      <div className="button-footer">
        <button className="hapus" onClick={deleteHandle}>
          Delete
        </button>
        <Link to={`/edit-post/${id}`}>
          <button>Edit</button>
        </Link>
      </div>
    </>
  );
};

export default DetailPost;
