import React from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./Post.scss";

const Post = ({ title, post_id, content }) => {
  return (
    <>
      <Card
        className="my-4 p-2 rounded text-center mb-3 pt-5"
        style={{ border: "none" }}
      >
        <div className="post">
          <div className="post-title">
            <h3>{title}</h3>
          </div>
          <Card.Body className="rounded text-white text-left">
            <div className="post-title">
              <p>{content}</p>
            </div>
          </Card.Body>
          <Link to={`/detail-post/${post_id}`}>
            <strong>Read More</strong>
          </Link>
        </div>
      </Card>
    </>
  );
};

export default Post;
