import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { createPostAsync } from "../../redux/action/createPostAction";
import "./NewPost.scss";

const NewPost = () => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

  const dispatch = useDispatch();

  const handleCreate = (e) => {
    e.preventDefault();

    dispatch(createPostAsync(title, content));
  };

  return (
    <div className="your-post">
      <form onSubmit={handleCreate}>
        <div className="post-details">
          <h3>Create New Post</h3>
          <div className="form">
            <div className="left-form">
              <label htmlFor="">
                <p>Title*</p>
              </label>
              <input
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
            </div>
            <div className="right-form">
              <label htmlFor="">
                <p>Content*</p>
              </label>
              <textarea
                name="decription"
                value={content}
                onChange={(e) => setContent(e.target.value)}
                id=""
                placeholder="Content Decription "
              ></textarea>
            </div>
          </div>
        </div>
        <div className="button-footer">
          <button type="submit">Create</button>
        </div>
      </form>
    </div>
  );
};

export default NewPost;
