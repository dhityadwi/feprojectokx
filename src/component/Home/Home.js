import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { postsAsync } from "../../redux/action/postsAction";
import Loader from "../Post/Loader";
import Post from "../Post/Post";

const Home = () => {
  const dispatch = useDispatch();

  // const [post, setPost] = useState([]);
  const [loading, setLoading] = useState(true);

  const { data } = useSelector((state) => state.post);
  console.log(data);

  useEffect(() => {
    dispatch(postsAsync());
    setLoading(false);
  }, []);
  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <Row>
          {data.map((item) => (
            <Col
              key={item.id}
              xs={12}
              sm={12}
              md={4}
              lg={3}
              xl={3}
              className="mt-5"
            >
              <Post
                title={item.title}
                post_id={item.id}
                content={item.content}
              />
            </Col>
          ))}
        </Row>
      )}
    </>
  );
};

export default Home;
