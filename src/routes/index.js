import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import DetailPostPage from "../pages/DetailPostPage";
import EditPostPage from "../pages/EditPostPage";
import Homepages from "../pages/Homepages";
import NewPostPage from "../pages/NewPostPage";

const Routess = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Homepages}></Route>
        <Route exact path="/new-post" component={NewPostPage}></Route>
        <Route exact path="/detail-post/:id" component={DetailPostPage}></Route>
        <Route exact path="/edit-post/:id" component={EditPostPage}></Route>
      </Switch>
    </Router>
  );
};

export default Routess;
