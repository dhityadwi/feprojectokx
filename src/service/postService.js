import axios from "axios";

export const getPostsService = async () => {
  const url = "https://limitless-forest-49003.herokuapp.com/posts";

  try {
    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    return response.json();
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const showPostService = (id) => {
  const url = `https://limitless-forest-49003.herokuapp.com/posts/${id}`;

  try {
    const response = axios(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    return response;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const createPostsService = async (title, content) => {
  const url = "https://limitless-forest-49003.herokuapp.com/posts";

  const data = {
    title,
    content,
  };

  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    return response.json();
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const putPostService = async (title, content, id) => {
  const url = `https://limitless-forest-49003.herokuapp.com/posts/${id}`;

  const data = {
    title,
    content,
  };

  try {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    return response.json();
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const deletePostService = async (title, content, id) => {
  const url = `https://limitless-forest-49003.herokuapp.com/posts/${id}`;
  const data = {
    title,
    content,
  };

  try {
    const response = await fetch(url, {
      method: "DEL",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    return response.json();
  } catch (error) {
    console.log(error);
    throw error;
  }
};
