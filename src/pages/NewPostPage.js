import React from "react";
import Header from "../component/Header/Header";
import NewPost from "../component/NewPost/NewPost";

const NewPostPage = () => {
  return (
    <div>
      <Header />
      <NewPost />
    </div>
  );
};

export default NewPostPage;
