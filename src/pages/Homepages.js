import React from "react";
import Header from "../component/Header/Header";
import Home from "../component/Home/Home";

const Homepages = () => {
  return (
    <div>
      <Header />
      <Home />
    </div>
  );
};

export default Homepages;
