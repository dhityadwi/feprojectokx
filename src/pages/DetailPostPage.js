import React from "react";
import Header from "../component/Header/Header";
import DetailPost from "../component/Post/DetailPost";

const DetailPostPage = () => {
  return (
    <div>
      <Header />
      <DetailPost />
    </div>
  );
};

export default DetailPostPage;
