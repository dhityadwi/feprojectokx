import React from "react";
import EditPost from "../component/EditPost/EditPost";
import Header from "../component/Header/Header";

const EditPostPage = () => {
  return (
    <div>
      <Header />
      <EditPost />
    </div>
  );
};

export default EditPostPage;
